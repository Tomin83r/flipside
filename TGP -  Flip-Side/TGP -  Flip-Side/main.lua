
function love.load()
  
  numberLevels = 4
  
  swapRob = 1--switch between startPos and Waypoints for robots
  
  BlackBackground = love.graphics.newImage("sprites/BlackBackground.png")
  WhiteBackground = love.graphics.newImage("sprites/WhiteBackground.png")

  leftArrow = love.graphics.newImage("sprites/Left_Arrow.png")
  rightArrow = love.graphics.newImage("sprites/Right_Arrow.png")
  
  blackPlatform = love.graphics.newImage("sprites/Black_Floor.png")
  whitePlatform = love.graphics.newImage("sprites/White_Floor.png")
  
  blackWall = love.graphics.newImage("sprites/BlackWall.png")
  whiteWall = love.graphics.newImage("sprites/WhiteWall.png")
  
  ground = love.graphics.newImage("sprites/Bottom_Floor.png")
  
  RobotTex = love.graphics.newImage("sprites/RobotSingleCentre.png") -- 65 height, 54 width
  
  backgroundQuad = love.graphics.newQuad(1,1,1280/2,720/2,1280/2,720/2)

  ScreenWidth = 1280/2
  ScreenHeight = 720/2
  gravity = 5

  debugKeyDown = false 
  flipable = true

  Player = {} 
  Player.Tex = love.graphics.newImage("sprites/ChuckBlackRight.png" ) 
  Player.PosY = 10
  Player.PosX = 50
  Player.Score = 0 
  Player.Height = 60
  Player.Width = 26
  Player.Color = "Black" -- use this for world colour 
  Player.Direction = 2 -- 1= left 2= right
 
 
  
  Player.ShadowTex = love.graphics.newImage("sprites/BlackShadow.png") 
  Player.ShadowPosY = 100
  Player.ShadowPosX = 500
  
  --ExitDoor = {}
  ExitDoorTex = love.graphics.newImage("sprites/WhiteExitDoor.png")
  --ExitDoor.PosX = 300
  --ExitDoor.PosY = 120
  --ExitDoor.Width = 74
 -- ExitDoor.Height = 64
  
  -- Game states 
  GameState = "Game"
  WorldColor = "White"
  DebugState = false
  turnTick = 1
  CurrentLevel = 1  -- do this for each level and then when we restart we grab what the current level is to reload
  
  SetPositions() -- Sets the positions for level 1 to intialize the game
  
  -- Audio 
  deathsound = love.audio.newSource("audio/Thud.wav", "static") 
  music = love.audio.newSource("audio/Delightful.mp3") 
  ScoreSound = love.audio.newSource("audio/Chime.wav", "static")

end

function love.draw()
  
  if WorldColor == "White" then
    love.graphics.draw(WhiteBackground, backgroundQuad, 0, 0)   
  elseif WorldColor == "Black" then
    love.graphics.draw(BlackBackground, backgroundQuad, 0, 0)
  end
  
  
  if (GameState == "MainMenu") then
    Main_Menu_Screen()
  end
  if (GameState == "Game") then
      Game_Screen()
  end
  
  if (GameState == "GameOver") then 
      Game_Over_Screen()
  end
end

function Main_Menu_Screen()
  love.graphics.print("Press the Enter key\n To start",45,300)
end

function Game_Screen()
  
   love.graphics.draw(Player.ShadowTex, Player.ShadowPosX, Player.ShadowPosY)
   love.graphics.draw(Player.Tex, Player.PosX, Player.PosY)
   --love.graphics.draw(Robot.Tex, Robot.PosX, Robot.PosY)
   
    for i,v in ipairs(Robot) do 
      if v.Direction == 2 then
    
        RobotTex = love.graphics.newImage("sprites/RobotSingleCentre.png")
  
      elseif v.Direction == 1 then
  
        RobotTex = love.graphics.newImage("sprites/RobotLeft.png")
    
      elseif v.Direction == 3 then
  
      RobotTex = love.graphics.newImage("sprites/RobotRight.png")
    
      end
  
     love.graphics.draw(RobotTex, v.PosX, v.PosY) -- New 
    end
  --love.graphics.print(Player.Score,10,10) -- Time Based Score System.
  
    --World Properties
    
    for i,v in ipairs(KeyDoorWhiteTable) do   --new 
    love.graphics.draw(v.Tex, v.PosX, v.PosY)
  end
  
  for i,v in ipairs(KeyDoorBlackTable) do -- new
    love.graphics.draw(v.Tex, v.PosX, v.PosY)
  end
  
  for i,v in ipairs(Exit) do
   love.graphics.draw(ExitDoorTex, v.PosX, v.PosY) -- Changed from Blackdoubledoor texture gonna attach this world colour code to the door itself
   end
   
   
  if WorldColor == "White" then
    
      for i,v in ipairs(Platform) do
  love.graphics.draw(blackPlatform, v.PosX, v.PosY)
  end
     
     for i,v in ipairs(Wall) do 
     love.graphics.draw(blackWall, v.PosX, v.PosY)
    end
    
     
  elseif WorldColor == "Black" then
    
       for i,v in ipairs(Platform) do
  love.graphics.draw(whitePlatform, v.PosX, v.PosY)
end

  for i,v in ipairs(Wall) do 
     love.graphics.draw(whiteWall, v.PosX, v.PosY)
    end
     
     
  end
  
  --love.graphics.draw(blackKeyDoor, 250,20)
  love.graphics.draw(ground, 20, 350) 
  
  --Movement Arrows
  love.graphics.draw(leftArrow, 0, 0)
  love.graphics.draw(rightArrow, 620, 0)
  
  
  end

function SetPositions()
    PlatformCount = 0
    WhiteDoorCount = 0
    BlackDoorCount = 0
    WallCount = 0
    ExitCount = 0
    RobotCount = 0
    Player.PosX = 50
    Player.PosY = 20
    
    WorldColor = "White"
    Player.Color = "Black" 
    
    -- RobotCount = 0 -- To implement
    
    level = tostring(CurrentLevel)
    getmetatable("").__add=function(s1,s2) return s1..s2 end -- Can use the .. Operator instead but all this does is Make + take the string before it and the string after it and then concatenate them.
  PlatPositions = {}
 for line in love.filesystem.lines("Level" + level + "/PlatformPositions.txt") do 
   for x, y in line:gmatch("(.-):(.*)") do
      local tempPositions = {}
     tempPositions.x = x
    tempPositions.y = y
  
     PlatformCount = PlatformCount + 1
     table.insert(PlatPositions,tempPositions)
     end
  end
  
    WhiteDoorPositions = {}
 for line in love.filesystem.lines("Level" + level + "/WhiteDoorPositions.txt") do
   for x, y in line:gmatch("(.-):(.*)") do
      local tempPositions = {}
     tempPositions.x = x
    tempPositions.y = y
      WhiteDoorCount = WhiteDoorCount + 1
     table.insert(WhiteDoorPositions,tempPositions)
     end
  end
  
     BlackDoorPositions = {}
 for line in love.filesystem.lines("Level" + level + "/BlackDoorPositions.txt") do
   for x, y in line:gmatch("(.-):(.*)") do
      local tempPositions = {}
     tempPositions.x = x
    tempPositions.y = y
    BlackDoorCount = BlackDoorCount + 1
     table.insert(BlackDoorPositions,tempPositions)
     end
  end
  
  WallPositions = {}
 for line in love.filesystem.lines("Level" + level + "/WallPositions.txt") do
   for x, y in line:gmatch("(.-):(.*)") do
      local tempPositions = {}
     tempPositions.x = x
    tempPositions.y = y
  
   WallCount = WallCount + 1
  
     table.insert(WallPositions,tempPositions)
     end
  end
  
    ExitPosition = {}
 for line in love.filesystem.lines("Level" + level + "/ExitPosition.txt") do
   for x, y in line:gmatch("(.-):(.*)") do
      local tempPositions = {}
     tempPositions.x = x
    tempPositions.y = y
  
   ExitCount = ExitCount + 1
  
     table.insert(ExitPosition,tempPositions)
     end
  end 
  
    RobotPosition = {}
    RobotWayPointsInput = {}
 for line in love.filesystem.lines("Level" + level + "/RobotPosition.txt") do
   
   if (swapRob == 1) then
     for x, y in line:gmatch("(.-):(.*)") do
        local tempRobots = {}
       tempRobots.x = x
      tempRobots.y = y
      RobotCount = RobotCount + 1
       table.insert(RobotPosition,tempRobots)
      end
    end
    if (swapRob == 2) then
      for left, right in line:gmatch("(.-):(.*)") do
        local tempWay = {}
       tempWay.left = left
      tempWay.right = right
      
       table.insert(RobotWayPointsInput,tempWay)
      end
    
    end
    if (swapRob == 1) then
      swapRob =2
    else 
      swapRob =1
    end
  end
  
 
  
  
   Platform = {}
 for i=1,PlatformCount do
  local tempPlatform = {}
 
   tempPlatform.PosX = tonumber(PlatPositions[i].x)
   tempPlatform.PosY = tonumber(PlatPositions[i].y)
   tempPlatform.Width = 80
   tempPlatform.Height = 5
  table.insert(Platform,tempPlatform) 
end   

  KeyDoorWhiteTable = {}
  for i=1,WhiteDoorCount do
  local KeyDoorWhite = {}

    KeyDoorWhite.PosX =  tonumber(WhiteDoorPositions[i].x)
    KeyDoorWhite.PosY =  tonumber(WhiteDoorPositions[i].y)
    KeyDoorWhite.Width = 30
    KeyDoorWhite.Height = 60
    KeyDoorWhite.Tex = love.graphics.newImage("sprites/WhiteKeyDoor.png")
    KeyDoorWhite.Color = "White"
    
  table.insert(KeyDoorWhiteTable,KeyDoorWhite)
  end

  KeyDoorBlackTable = {}
  for i=1,BlackDoorCount do -- Counts to determine how large the for loop should be
  local KeyDoorBlack = {}
  
    KeyDoorBlack.PosX = tonumber(BlackDoorPositions[i].x)
    KeyDoorBlack.PosY = tonumber(BlackDoorPositions[i].y)
    KeyDoorBlack.Width = 30
    KeyDoorBlack.Height = 60
    KeyDoorBlack.Tex = love.graphics.newImage("sprites/BlackKeyDoor.png")
    KeyDoorBlack.Color = "Black"
      
  table.insert(KeyDoorBlackTable,KeyDoorBlack)
  end

Wall = {}
  for i=1,WallCount do
  local tempWall = {}
 
   tempWall.PosX = tonumber(WallPositions[i].x)
   tempWall.PosY = tonumber(WallPositions[i].y)
   tempWall.Width = 5
   tempWall.Height = 80
  table.insert(Wall,tempWall)
end   

Exit = {}
 for i=1,ExitCount do
  local tempExit = {}
 
   tempExit.PosX = tonumber(ExitPosition[i].x)
   tempExit.PosY = tonumber(ExitPosition[i].y)
   tempExit.Width = 74
   tempExit.Height = 64
  table.insert(Exit,tempExit) 
end   

Robot = {}
 for i=1,RobotCount do
  local tempRobots = {}
 
   tempRobots.PosX = tonumber(RobotPosition[i].x)
   tempRobots.PosY = tonumber(RobotPosition[i].y)
   tempRobots.Left = tonumber(RobotWayPointsInput[i].left)
   tempRobots.Right = tonumber(RobotWayPointsInput[i].right)
   tempRobots.Width = 54
   tempRobots.Height = 65
   tempRobots.Direction = 3
  table.insert(Robot,tempRobots) 
end   

    

end


  

function Game_Over_Screen()
  
  -- love.graphics.print("Score -",720/6, 250) -- Change this to be when you die it comes up with a prompt to restart (a button perhaps) and a thing saying that you died - could create an art asset for this.
  love.graphics.print("Press the screen to restart", 220, 100)
  love.graphics.draw(RobotTex, 220,120) --replace with a restart button
end


function CheckCollision(x1,y1,w1,h1, x2,y2,w2,h2) -- Collision Code
  return x1 < x2+w2 and
         x2 < x1+w1 and
         y1 < y2+h2 and
         y2 < y1+h1
end

function love.update(dt)

 -- Movement with keys/phone (Keys Done)
 for i,v in ipairs(Robot) do

 -- robot movement
  if ((v.Direction == 3) and (v.PosX < v.Right)) then
    v.PosX = v.PosX + 2
    turnTick = 0

  elseif ((v.PosX >= v.Right) and (turnTick < 10)) then
    v.Direction = 2
    turnTick = (turnTick + 1)
  
  elseif ((v.PosX >= v.Right) and (turnTick >= 10) and (v.Direction == 2)) then
    v.Direction = 1
    
    
  elseif ((v.Direction == 1) and (v.PosX > v.Left)) then
    v.PosX = v.PosX - 2
    turnTick = 0

  elseif ((v.PosX <= v.Left) and (turnTick < 10)) then
    v.Direction = 2
    turnTick = (turnTick + 1)
  
  elseif ((v.PosX <= v.Left) and (turnTick >= 10) and (v.Direction == 2)) then
    v.Direction = 3
    
  end
  end

  if Player.PosX < ScreenWidth/2 then
     Player.ShadowPosX = ((ScreenWidth) - Player.PosX - Player.Width)
  end
  
  if Player.PosX > ScreenWidth/2 then
     Player.ShadowPosX = 0 + ((ScreenWidth - Player.PosX) - Player.Width)
  end
  
  Player.ShadowPosY = Player.PosY
  
  if WorldColor == "White" then
    ExitDoorTex = love.graphics.newImage("sprites/BlackExitDoor.png")
    Wall.Tex = love.graphics.newImage("sprites/BlackWall.png") ---- new
  else if WorldColor == "Black" then
    ExitDoorTex = love.graphics.newImage("sprites/WhiteExitDoor.png")
    Wall.Tex = love.graphics.newImage("sprites/WhiteWall.png")
  end
end

 
  Collision()
  Gravity()
  KeyboardControls()

 
 ChuckSpriteChange()
 PassDoors()
  
end

function Collision()
  
  for i,v in ipairs(Robot) do
   hitTest = CheckCollision(Player.PosX,Player.PosY,Player.Width,Player.Height,v.PosX, v.PosY, v.Width, v.Height)
   
    if (hitTest) then
     
   GameState = "GameOver"
  -- re intialize position --will change based upon level so case statement or read from file
  Player.PosY = 15
  Player.PosX = 27
  Player.Color = "Black" 
   
  -- reset world color
  WorldColor = "White"
   
end
 end
 
  
 
 -- Door Collision tests on the shadow so that you dont flip into doors (would result in pushing you to 1 side of the door otherwise, allowing you to buypass inteded areas)
for i,v in ipairs(KeyDoorBlackTable) do
 DoorTestBlack = CheckCollision(Player.ShadowPosX,Player.ShadowPosY,Player.Width,Player.Height,KeyDoorBlackTable[i].PosX,KeyDoorBlackTable[i].PosY,KeyDoorBlackTable[i].Width,KeyDoorBlackTable[i].Height)
end

for i,v in ipairs(KeyDoorWhiteTable) do
 DoorTestWhite = CheckCollision(Player.ShadowPosX,Player.ShadowPosY,Player.Width,Player.Height,KeyDoorWhiteTable[i].PosX,KeyDoorWhiteTable[i].PosY,KeyDoorWhiteTable[i].Width,KeyDoorWhiteTable[i].Height)
end

if (DoorTestBlack) or (DoorTestWhite) then
  Flipable = false
end

if not (DoorTestWhite) and not (DoorTestBlack) then
   Flipable = true
end

for i,v in ipairs(Exit) do
ExitDoorTest = CheckCollision(Player.PosX,Player.PosY,Player.Width,Player.Height,v.PosX,v.PosY,v.Width,v.Height)
end
if (ExitDoorTest) then
  
  if (CurrentLevel == numberLevels ) then
  
    --end game
  
  else  
    CurrentLevel = CurrentLevel + 1
  end
   -- Hard coded currently as we need to reset our play position so you dont keep triggering it -- Also need to set what is the end level -- CurrentLevel + 1
-- Player start positions + robot start position + worldColour reset will be in the set positions method

   SetPositions() --move in with (currentlevel =+ 1) after end screen is done
   
end

 for i,v in ipairs(Wall) do
  WallTest = CheckCollision(Player.PosX, Player.PosY, Player.Width, Player.Height, v.PosX, v.PosY, v.Width, v.Height)

 if WallTest == true then
         if Player.Direction == 1 then
        
         Player.PosX = v.PosX + 6

        elseif Player.Direction == 2 then
      
        Player.PosX = v.PosX - 27
        
        end
      
      end
  end --


end

function Gravity() -- Only Fall when not stood on a platform or the ground
  Player.PosY = Player.PosY + gravity  -- Working gravity
  hitTestGrav = false
   for i,v in ipairs(Platform) do
  hitTest = CheckCollision(Player.PosX, Player.PosY, Player.Width, Player.Height, v.PosX, v.PosY, v.Width, v.Height)
  
 
 if hitTest == true then
     hitTestGrav = true
     Player.PosY = v.PosY - Player.Height
  end --
  
  
  if(hitTestGrav) then
    gravity = 0
    
  else 
  gravity = 5
 
 end
 end


 if(Player.PosY == 290) then
   gravity = 0
 end
 
end


function KeyboardControls() -- Press "1" for debugging control (Y movement , Disable Collision on Enemys etc.)
  
  if GameState == "Game" then

    if love.keyboard.isDown("d") and Player.PosX < 582 then  -- Includes both player movement and stops them from walking of the screen
      Player.PosX = Player.PosX + 3
       Player.Direction = 2
    end
  
    if love.keyboard.isDown("a") and Player.PosX > 20 then
      Player.PosX = Player.PosX - 3
       Player.Direction = 1
    end
   
  if love.keyboard.isDown("space") and SpaceKeyDown == false and Flipable == true then -- Player Flip Mechanic Code
    -- Color flip + Tex Swap
    if (Player.Color == "Black") then
      Player.Color = "White"
      WorldColor = "Black"
  
    else if (Player.Color == "White") then
      Player.Color = "Black"
      WorldColor = "White"
    end
    end
      Player.PosX = Player.ShadowPosX
      SpaceKeyDown = true
  
end
end
  
  
   
    if not love.keyboard.isDown("space") then
   SpaceKeyDown = false
  end
  
   if love.keyboard.isDown("escape") then
    love.event.quit()  
  end
  
  if love.keyboard.isDown("enter") and GameState == "MainMenu" then
     GameState = "Game"
  end
  
  function love.mousepressed(x,y,button,istouch) -- restart the game upon death
    if button == 1 and GameState == "GameOver" then
      -- insert what level u were on when you died and then for gamestate you load up that level instead of just the main level
    GameState = "Game"
    --Robot.PosY = 
    end
  end
  
  
   -- Debugging Controls
   DebugArea()
   --DebugState = false
 
  
  
end


function ChuckSpriteChange()
 if Player.Color == "Black" then
    Player.ShadowTex = love.graphics.newImage("sprites/BlackShadow.png")
      if Player.Direction == 1 then
       
        Player.Tex = love.graphics.newImage("sprites/ChuckBlackLeft.png")
        
      elseif Player.Direction == 2 then
        
        Player.Tex = love.graphics.newImage("sprites/ChuckBlackRight.png")
        
      end
    
  elseif Player.Color == "White" then
    Player.ShadowTex = love.graphics.newImage("sprites/WhiteShadow.png")
    if Player.Direction == 1 then
      
      Player.Tex = love.graphics.newImage("sprites/ChuckWhiteLeft.png")
      
    elseif Player.Direction == 2 then
      
      Player.Tex = love.graphics.newImage("sprites/ChuckWhiteRight.png")
       
    end
    
  end
end

function PassDoors()
  
  --if door is white 
  for i,v in ipairs(KeyDoorWhiteTable) do
    hitTestKey = CheckCollision(Player.PosX,Player.PosY,Player.Width,Player.Height,v.PosX, v.PosY, v.Width, v.Height)
    if ((hitTestKey) and (Player.Color == v.Color))then
      if Player.Direction == 1 then
        
         Player.PosX = v.PosX - 27
      
      elseif Player.Direction == 2 then
      
        Player.PosX = v.PosX + 31
      
      end
    end
    
    if ((hitTestKey) and  (Player.Color ~= v.Color))then
      
      if Player.Direction == 1 then
        
        Player.PosX = v.PosX + 31
      
      elseif Player.Direction == 2 then
     
        Player.PosX = v.PosX - 27
      
      end
    
    end 
  end
  
  
  --if door is black 
  for i,v in ipairs(KeyDoorBlackTable) do
    hitTestKey = CheckCollision(Player.PosX,Player.PosY,Player.Width,Player.Height,v.PosX, v.PosY, v.Width, v.Height)
    if ((hitTestKey) and (Player.Color == v.Color))then
      if Player.Direction == 1 then
        
         Player.PosX = v.PosX - 27
      
      elseif Player.Direction == 2 then
      
        Player.PosX = v.PosX + 31
      
      end
    end
    
    if ((hitTestKey) and  (Player.Color ~= v.Color))then
      
      if Player.Direction == 1 then
        
        Player.PosX = v.PosX + 31
      
      elseif Player.Direction == 2 then
     
        Player.PosX = v.PosX - 27
      
      end
    
    end 
  end
end


function DebugArea()
   -- Debugging Controls
   
   --DebugState = false
    if love.keyboard.isDown("1") and DebugState == false and debugKeyDown == false then
      DebugState = true
      debugKeyDown = true
    end
  
    if love.keyboard.isDown("1") and DebugState == true and debugKeyDown == false then
      DebugState = false
      debugKeyDown = true
    end
   
    if love.keyboard.isDown("w") and DebugState == true then
      Player.PosY = Player.PosY - 10
    end
  
    if love.keyboard.isDown("s")  and DebugState == true then
      Player.PosY = Player.PosY + 10
    end
  
    if not love.keyboard.isDown("1") then
    debugKeyDown = false
    end
  
    if love.keyboard.isDown("g")  and DebugState == true then
      gravity = 0 -- hold down G to disable gravity
    end
end